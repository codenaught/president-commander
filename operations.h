#ifndef OPERATIONS_H
#define OPERATIONS_H

#include <string>
std::string get_time();
std::string get_weather();
std::string get_random_number();
std::string run_browser();
std::string run_im();
std::string sub_menu_test();

#endif
