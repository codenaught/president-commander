#ifndef MAIN_H
#define MAIN_H

#include <string>

/* Uses the input stream and returns the response given by the user.
 */
std::string get_question();

/* Simply traverses through the linked list menu.
 */
void menu(Menu * tree, int level);

/* Takes in the pieces to construct a menu item, including the delegate function to be called 
 *     when selected, then adds the menu to the linked list
 */
Item * add(Menu * source, int id, std::string (*handler)(), std::string desc, std::string keyword, bool output_result = true, Menu * dest = NULL);

/* Adds a go back or exit to the end of the menu
 */
Item * add_back(Menu * source, Menu * dest);

/* Displays the item. Calls the handler function for the menu item.
 */
void displayItem(Item * item);

/* Displays the menu. Show the main menu with all the root menu items.
 */
void displayMenu(Menu * menu);

/* Performs a simple search through the linked list, for the key.
 *     Can find matches using both the id and keyword.
 */
Item * search_list(Menu * tree, std::string key);

/* Constructs the menu. Explicitely uses the add function to build the menu
 *    one by one. Returns a pointer the newly created linkd list.
*/
Menu * build_tree();

 /* Loops through the menu, calls displayMenu, search_list, get_question and displayItem.
 */
void menuLoop(Menu * menu);
#endif
