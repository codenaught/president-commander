#include <iostream>
#include <string>
#include <time.h>
#include <sstream>
#include "boost/date_time/posix_time/posix_time.hpp"
#include "operations.h"

using namespace boost::posix_time;

std::string get_time()
{
	ptime t(second_clock::local_time());
	time_duration time = t.time_of_day();
	std::string time_str = "The Current Time\n" + to_simple_string(time);

	return time_str;
}

std::string run_browser()
{
	system("firefox -new-tab http://www.duckduckgo.com");
}

std::string run_im()
{
	system("pidgin");
}

std::string get_weather()
{
	return "The Current Weather ";
}

std::string get_random_number()
{
	int num = rand();
	std::string num_str;
	std::stringstream ss;
	ss << num;

	return ss.str();
}

std::string sub_menu_test()
{
	return "Sub Menu Description!";
}
