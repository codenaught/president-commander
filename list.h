#ifndef ITEM_H
#define ITEM_H

struct Item;
struct Menu;

struct Item
{
	int id;
	std::string (*handler)();	
	std::string desc;
	std::string keyword;
	Item * next;
	// The Menu to show after the item is selected.
	Menu * menu;
	
	// Options
	bool output_result;

};

struct Menu
{
	Item * first;
	Item * last;
	int size;
};

#endif
