#include <iostream>
#include <string>
#include <sstream>
#include <stdlib.h>
#include "operations.h"
#include "list.h"
#include "main.h"

using namespace std;

int main()
{

	unsigned int seed = (unsigned int)time(NULL);
	srand(seed);
	Menu * tree = build_tree();	

	while (1)
	{
		menuLoop(tree);
	}

	return 0;
}

void menuLoop(Menu * menu)
{
	if (menu == NULL)
	{
		exit(0);
	}
		
	displayMenu(menu);
	string ques = get_question();
	Item * found = search_list(menu, ques);
	if (found != NULL)
	{
		displayItem(found);
	}
}

void displayMenu(Menu * tree)
{
	menu(tree, 0);
	cout << endl << "Input: ";
}

Menu * build_tree()
{
	Menu * tree = new Menu;
	tree->first = NULL;
	tree->last = NULL;
	tree->size = 0;
	int* index = &tree->size;

	add(tree, *index, &get_time, "Get Current Time", "time");
	add(tree, *index, &get_weather, "Get Current Weather", "weather");
	add(tree, *index, &get_random_number, "Get Random Number", "random");
	add(tree, *index, &run_browser, "Open Web Browser", "browser", false);
	add(tree, *index, &run_im, "Open Pidgin", "im", false);
	
	Item * test_menu = add(tree, *index, &sub_menu_test, "Open Test Sub Menu", "menu");

	// Sub Menu Test...
	Menu * sub_test = new Menu;
	sub_test->first = sub_test->last = NULL;
	sub_test->size = 0;
	test_menu->menu = sub_test;
	add(sub_test, 0, &get_time, "Show me the time!", "time", true, sub_test);
	add_back(sub_test, tree);

	// Leads to an exit from the program when selected.
	add_back(tree, NULL);

	return tree;
}

void displayItem(Item * item)
{
	if (item->output_result)
		cout << item->handler() << endl;
	else if (item->handler != NULL)
		item->handler();

	if (item->menu != NULL)
	{
		menuLoop(item->menu);
	}
	/* No handler set, and we already know menu is NULL, meaning an 
		exit from the program has been selected.*/
	else if (item->handler == NULL)
	{
		menuLoop(NULL);
	}
}

void menu(Menu * tree, int level)
{
	Item * node = tree->first;
	cout << endl;
	while (node != NULL)
	{
		cout << node->id << ": " << node->desc << endl;
		node = node->next;
	}
}

Item* add(Menu * tree, int id, string (*handler)(), string desc, string keyword, bool output_result, Menu * dest)
{
	Item * item = new Item;
	item->handler = handler;
	item->desc = desc + " (" + keyword + ")";
	item->id = id;
	item->next = NULL;
	item->keyword = keyword;
	item->output_result = output_result;
	item->menu = dest;

	if (tree->size == 0)
	{
		tree->first = item;
		tree->last = item;
	}
	else
	{
		tree->last->next = item;
		tree->last = tree->last->next;
	}

	tree->size++;
	
	return item;
}

Item * add_back(Menu * source, Menu * dest)
{
	Item * item = new Item;
	item->handler = NULL;
	item->id = -1;
	item->next = NULL;
	item->menu = dest;
	item->keyword = "back";
	item->desc = "Go Back";
	item->output_result = false;

	// Prereq: Assume add_back is called for menu with size >= 1.
	if (source->size == 0)
	{
		exit(-1);
	}
	source->last->next = item;
	source->last = source->last->next;

	return item;
}

Item * search_list(Menu * tree, string key)
{
	Item * node = tree->first;
	int id;

	while (node != NULL)
	{
		stringstream ss(key);
		ss >> id;
		// Set our fail to a -2 so our menu supports 0 as an id.
		if (ss.fail())
			id = -2;
		if (key == node->keyword || id == node->id)
		{
			return node;
		}
		node = node->next;
	}
	return NULL;
}

string get_question()
{
	string ques = "";
	cin >> ques;

	return ques;
}
